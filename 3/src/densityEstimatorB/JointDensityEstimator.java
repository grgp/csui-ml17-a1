package densityEstimatorB;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;

public class JointDensityEstimator {
	public static void main(String[] args) throws FileNotFoundException {
		String fileName = "car.data.txt";
		File file = new File(fileName);
		Scanner sc = new Scanner(file);
		
		// get number of columns
		String line = sc.nextLine();
		int nFeature = (line.split(",")).length;
		ArrayList<ArrayList<String>> features = new ArrayList<ArrayList<String>>();
		ArrayList<String> outputVector = new ArrayList<String>();
		ArrayList<Integer> passed = new ArrayList<Integer>();
		HashMap<ArrayList<String>, Integer> counts = new HashMap<ArrayList<String>, Integer>();
		HashMap<ArrayList<String>, Integer> jointDensity = new HashMap<ArrayList<String>, Integer>();
		
		while (sc.hasNextLine()) {
			line = sc.nextLine();
			String[] line_array = line.split(",");
			String[] row_features = Arrays.copyOfRange(line_array, 0, line_array.length); // or .length-1
			features.add(new ArrayList<String>(Arrays.asList(row_features)));
			outputVector.add(line_array[line_array.length-1]);
		}
		
		for (int i = 0; i < features.size(); i++) {
			counts.put(features.get(i), 0);
		}
		
		for (int i = 0; i < features.size(); i++) {
			ArrayList<Integer> equivalents = new ArrayList<Integer>();
			passed.add(i);
			counts.put(features.get(i), 1);
			for (int j = 0; j < features.size(); j++) {
				if (!passed.contains(j) && features.get(i).equals(features.get(j))) {
					equivalents.add(j);
					counts.put(features.get(j), counts.get(features.get(j))+1);
				}
			}
			jointDensity.put(features.get(i), counts.get(features.get(i)));
		}
		
		for(Map.Entry<ArrayList<String>,Integer> entry : jointDensity.entrySet()) {
		  ArrayList<String> key = entry.getKey();
		  Integer value = entry.getValue();
		  BigDecimal bvalue = new BigDecimal(value);
		  BigDecimal bsize = new BigDecimal(features.size());
		  BigDecimal probability_val = bvalue.divide(bsize, 10, BigDecimal.ROUND_HALF_UP);
		  System.out.println(key.toString() + " => " + probability_val);
		}
		
	}
}