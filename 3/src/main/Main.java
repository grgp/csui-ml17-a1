package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import id3.AlgoID3;
import kFoldValidator.KFoldValidator;
import naiveBayesClassifier.BayesClassifier;

public class Main {
	public static void main(String[] args) throws IOException {
		
		String fileName = "car.data.txt";
		File file = new File(fileName);
		Scanner s = new Scanner(file);
		
		//get number column
		String[] line = s.nextLine().split(",");
		int nFeature = line.length;
		
		
		//Decision Tree Initialization
		AlgoID3 dTreeClassifier = new AlgoID3(line, "class_values");
		//Bayes classifier initialization
		
		BayesClassifier bayes = new BayesClassifier(nFeature);
		System.out.print("Masukan jumlah K untuk validasi K-fold : ");
		
		s = new Scanner(System.in);
		int k = s.nextInt();
		
		
		System.out.println("Tunggu Klasifikasi ... ");
		KFoldValidator kfold = new KFoldValidator(fileName,k );
		System.out.println("Accuracy ID3 : "+ kfold.doValidation(dTreeClassifier));
		kfold = new KFoldValidator(fileName, k);
		System.out.println("Accuracy Bayes : "+ kfold.doValidation(bayes));
		
	}
}
