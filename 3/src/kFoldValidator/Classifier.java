package kFoldValidator;
/**
 * Interface for KFoldValidator Classifier
 * @author jefly
 *
 */
public interface Classifier {
	//training data from record
	public void learn(String[] data);
	//classify data
	public String classify(String[] data);
	//reset estimator
	public void reset();
}
