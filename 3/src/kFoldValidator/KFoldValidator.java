package kFoldValidator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
/**
 * K-Fold Validator implementation
 * @author jefly
 *
 */
public class KFoldValidator {
	//line position
	int k;
	//number of testing
	int i;
	//Dataset file
	File file;
	//number of line
	int nLine;
	
	/**
	 * Constructor
	 * @param file
	 * @param k
	 */
	public KFoldValidator(String file, int k) {
		this.file = new File(file);
		this.k = k;
		this.i = 0;
		countLine();
	}
	
	/**
	 * Calculate number of Line
	 */
	public void countLine() {
		Scanner in;
		try {
			in = new Scanner(file);
			while (in.hasNext()) {
				nLine++;
				in.next();
			}
			if(k>nLine){
				throw new Exception("K invalid");
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Do K Fold Validation
	 * @param classifier
	 * @return
	 */
	public double doValidation(Classifier classifier){
		long start = System.currentTimeMillis();
		i=0;
		double accuracy = 0;
		while(hasNext()){
			try {
				Scanner in = new Scanner(file);
				in.nextLine();
				int pos = 0;
				ArrayList<String[]> testingData = new ArrayList<>();
				while(in.hasNext()){
					//training set
					if(pos < (nLine/k)*i || pos >= (nLine/k)*(i+1)){
						classifier.learn(in.next().split(","));
					} else {
						testingData.add(in.next().split(","));
					}
					pos++;
				}
				//testing
				int nValid = 0; 
				for(String[] test : testingData){
					String result = classifier.classify(test);
					//System.out.println(Arrays.toString(test)+" = "+result);
					//System.out.println("+++++++++++++++++++++++++++++");
					if(result!=null && result.equals(test[test.length-1])){
						nValid++;
					}
				}
				//calculate accuration
				accuracy += nValid*1.0/testingData.size();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			classifier.reset();
			i++;
		}
		long end = System.currentTimeMillis();
		System.out.println("Time : "+(end-start)+" ms");
		//return means of accuration
		return accuracy*100.0/k;
	}
	
	/**
	 * check if k fold exceeding
	 * @return
	 */
	public boolean hasNext(){
		return i<k;
	}
}
