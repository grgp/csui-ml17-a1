package naiveBayesClassifier;

import java.util.HashMap;
import java.util.Map;
/**
 * Feature represented feature / column
 * @author jefly
 *
 */
public class Feature {
	//feature instance number
	private Map<String, Integer> feature;
	
	public Feature(){
		feature = new HashMap<>();
	}
	
	//add new Instance
	public void addData(String data){
		if(feature.containsKey(data)){
			feature.put(data, feature.get(data)+1);
		} else {
			feature.put(data, 1);
		}
	}
	
	//return map feature
	public Map<String,Integer> getFeature(){
		return this.feature;
	}
}
