package naiveBayesClassifier;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import kFoldValidator.Classifier;

/**
 * Categorical Naive Bayes Classifier
 * notes : class/category must be on the last column.
 * 
 * @author jefly
 *
 */
public class BayesClassifier implements Classifier{
	//Mapping Category and its feature
	Map<String, Feature[]> dataset;
	
	//Mapping Category and occurence number
	Map<String, Integer> nDataset;
	
	//number of column / feature
	int nFeature;
	
	/**
	 * Contructor
	 * @param nFeature
	 */
	public BayesClassifier(int nFeature){
		dataset = new HashMap<>();
		nDataset = new HashMap<>();
		this.nFeature = nFeature;
	}
	
	/**
	 * Learning from a record / row
	 */
	public void learn(String[] data){
		String category = data[data.length-1];
		//add new feature correspondent to category
		if(!dataset.containsKey(data[data.length-1])){
			dataset.put(category, new Feature[nFeature]);
		}
		//increment category occurence
		if(!nDataset.containsKey(category)){
			nDataset.put(category, 1);
		} else {
			nDataset.put(category, nDataset.get(category)+1);
		}
		
		//add instance to feature on a category
		for(int i = 0; i<data.length-1; i++){
			if(dataset.get(category)[i] == null){
				dataset.get(category)[i] = new Feature();
			}
			dataset.get(category)[i].addData(data[i]);
		}
	}
	
	/**
	 * Classify using MLE (Maximum Likelihood Estimator)
	 * Yp = argmax v (P(X1 = u1, X2 = u2, ... Xm = um | Y = v))
	 * 
	 *  where
	 *  
	 *  P(X1 = u1, X2 = u2, ... Xm = um | Y = v)
	 *  = P(X1=u1 | Y = v) * P(X2=u2 | Y = v).... * P(Xm = um) 
	 *  
	 */
	public String classify(String[] data){
		Iterator<String> itr = dataset.keySet().iterator();
		String maximumCategory = null;
		double maximumProbability = -1;
		//iterate all category to find maximum probability
		while(itr.hasNext()){
			String category = itr.next();
			double p = categoryProb(category, data);
			if(maximumProbability < p){
				maximumCategory = category;
				maximumProbability = p;
			}
		}
		return maximumCategory;
	}
	
	/**
	 * Calculate probability P(Xn = un | Y = v) 
	 * 
	 * @param category
	 * @param data
	 * @return
	 */
	public double categoryProb(String category, String[] data){
		double prob = 1;
		for(int i = 0; i<data.length-1;i++){
			//number of Feature correspondent on category
			int featureCount = 0;
			if(dataset.get(category)[i] != null){
				if(dataset.get(category)[i].getFeature().get(data[i]) != null){
					featureCount = dataset.get(category)[i].getFeature().get(data[i]);
				}
			}
			int categoryCount = nDataset.get(category);
			//System.out.println("P("+data[i]+"|"+category+") = "+featureCount+"/"+categoryCount);
			prob *= featureCount*1.0/categoryCount;
			
		}
		//System.out.println("===============================");
		return prob;
	}
	
	/**
	 * Reset table to running another testing
	 */
	public void reset(){
		dataset = new HashMap<>();
		nDataset = new HashMap<>();
	}
}
