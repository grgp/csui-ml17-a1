package densityEstimatorB;

import java.util.*;
import java.math.*;

public class JointDensityEstimator {

	public class Model {
		Map<List<String>, BigDecimal> probabilities;

		public Model(Map<List<String>, BigDecimal> ps) {
			this.probabilities = ps;
		}
	}

	public Model getModel(List<List<String>> data) {
		int nRecord = data.size();
		int nFeature = data.get(0).size();

		List<List<String>> transposedData = Util.transpose(data);

		List<Set<String>> listPossibleValues = new ArrayList();
		for (List<String> featureValues : transposedData) {
			Set<String> possibleValues = Util.getUnique(featureValues);
			listPossibleValues.add(possibleValues);
		}

		Map<List<String>, Integer> counts = new HashMap<>();
		for (List<String> record : data) {
			counts.put(record, counts.containsKey(record) ? counts.get(record) + 1 : 1);
		}

		Map<List<String>, BigDecimal> probabilities = new HashMap<>();
		for (List<String> record : counts.keySet()) {
			probabilities.put(record, new BigDecimal(counts.get(record)).divide(new BigDecimal(nRecord), 10, BigDecimal.ROUND_HALF_UP));
		}

		return new Model(probabilities);
	}

	public BigDecimal calculateProbability(List<String> record, Model model) {
		return model.probabilities.get(record);
	}

}
