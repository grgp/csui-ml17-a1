package densityEstimatorB;

import java.util.*;
import java.math.*;

public class NaiveDensityEstimator {

	public class Model {
		List<Map<String, BigDecimal>> probabilities;

		public Model(List<Map<String, BigDecimal>> ps) {
			this.probabilities = ps;
		}
	}

	public Model getModel(List<List<String>> data) {
		int nRecord = data.size();
		int nFeature = data.get(0).size();

		List<Map<String, BigDecimal>> probabilities = new ArrayList<>();

		List<List<String>> transposedData = Util.transpose(data);
		for (List<String> featureValues : transposedData) {
			Set<String> possibleValues = Util.getUnique(featureValues);

			Map<String, Integer> counts = new HashMap<>();
			for (String value : possibleValues) {
				counts.put(value, 0);
			}

			for (String value : featureValues) {
				counts.put(value, counts.get(value) + 1);
			}

			Map<String, BigDecimal> ps = new HashMap<>();
			for (String value : counts.keySet()) {
				ps.put(value, new BigDecimal(counts.get(value)).divide(new BigDecimal(nRecord), 10, BigDecimal.ROUND_HALF_UP));
			}
			probabilities.add(ps);
		}

		return new Model(probabilities);
	}

	public BigDecimal calculateProbability(List<String> record, Model model) {
		BigDecimal result = new BigDecimal(1);
		for (int i = 0; i < record.size(); i++) {
			Map<String, BigDecimal> ps = model.probabilities.get(i);
			result = result.multiply(ps.get(record.get(i)));
		}
		return result;
	}
}
