package densityEstimatorB;

import java.util.*;
import java.io.*;
import java.math.*;

public class Main {
	public static void main(String[] args) throws Exception {
		String fileName = "car.data.txt";
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		sc.nextLine();
		List<List<String>> data = new ArrayList();
		while (sc.hasNextLine()) {
			data.add(new ArrayList<>(Arrays.asList(sc.nextLine().split(","))));			
		}

		NaiveDensityEstimator nde = new NaiveDensityEstimator();
		JointDensityEstimator jde = new JointDensityEstimator();

		NaiveDensityEstimator.Model naiveDensityModel = nde.getModel(data);
		JointDensityEstimator.Model jointDensityModel = jde.getModel(data);
		
		System.out.printf("%60s => %12s %12s\n", "record", "NDE", "JDE");
		for (List<String> record : data) {
			BigDecimal p_nde = nde.calculateProbability(record, naiveDensityModel);
			BigDecimal p_jde = jde.calculateProbability(record, jointDensityModel);
			System.out.printf("%60s => %.10f %.10f\n", String.join(", ", record), p_nde, p_jde);
		}
	}
}