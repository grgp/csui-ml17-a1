package id3;


/* This file is copyright (c) 2008-2012 Philippe Fournier-Viger
* 
* This file is part of the SPMF DATA MINING SOFTWARE
* (http://www.philippe-fournier-viger.com/spmf).
* 
* SPMF is free software: you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.
* 
* SPMF is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
* A PARTICULAR PURPOSE. See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with
* SPMF. If not, see <http://www.gnu.org/licenses/>.
*/

/**
* This class represents a decision tree created by the ID3 algorithm.
 *
 * @see AlgoID3
 * @see Node
 * @see DecisionNode
 * @see ClassNode
 * @author Philippe Fournier-Viger
 */
public class DecisionTree  {
	// the list of attributes that was used to create the decision tree
	String []allAttributes;
	
	// a reference to the root node of the tree
	Node root = null;

	/**
	 * Print the tree to System.out.
	 */
	public void print() {
		System.out.println("DECISION TREE");
		String indent = " ";
		print(root, indent, "");
	}

	/**
	 * Print a sub-tree to System.out
	 * @param nodeToPrint the root note
	 * @param indent the current indentation
	 * @param value  a string that should be used to increase the indentation
	 */
	private void print(Node nodeToPrint, String indent, String value) {
		if(value.isEmpty() == false)
		System.out.println(indent + value);

		String newIndent = indent + "  ";

		// if it is a class node
		if(nodeToPrint instanceof ClassNode){
			// cast to a class node and print it
			ClassNode node = (ClassNode) nodeToPrint;
			System.out.println(newIndent + "  ="+ node.className);
		}else{
			// if it is a decision node, cast it to a decision node
			// and print it.
			DecisionNode node = (DecisionNode) nodeToPrint;
			System.out.println(newIndent + allAttributes[node.attribute] + "->");

			newIndent = 			newIndent + "  ";
			// then recursively call the method for subtrees
			for(int i=0; i< node.nodes.length; i++){
				print(node.nodes[i], newIndent, node.attributeValues[i]);
			}
		}
		
	}	
}
