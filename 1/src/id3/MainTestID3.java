package id3;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;

/**
 * Example of how to use ID3 from the source code.
 * @author Philippe Fournier-Viger (Copyright 2011)
 */
public class MainTestID3 {
	public static void main(String [] arg) throws IOException{
		// Read input file and run algorithm to create a decision tree
		AlgoID3 algo = new AlgoID3();
		// There is three parameters:
		// - a file path
		// - the "target attribute that should be used to create the decision tree
		// - the separator that was used in the file to separate values (by default it is a space)
		DecisionTree tree = algo.runAlgorithm("tennis.data.txt", "play", ",");
		algo.printStatistics();
		// print the decision tree:
		tree.print();
	}

	public static String fileToPath(String filename) throws UnsupportedEncodingException{
		URL url = MainTestID3.class.getResource(filename);
		 return java.net.URLDecoder.decode(url.getPath(),"UTF-8");
	}
}
